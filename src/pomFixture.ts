import { test as baseTest } from '@playwright/test'

type pages = {
    oldLessonCreationPage: OldLessonCreationPage;

}
const allPages = baseTest.extend<pages>({
    oldLessonCreationPage: async ({ page }, use) => {
        await use(new OldLessonCreationPage(page));
    }
})

export const test = allPages;
export const expect = allPages.expect